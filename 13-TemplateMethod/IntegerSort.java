public class IntegerSort extends SortAlgorithm{
    @Override
    public boolean lessThan(Object a, Object b){
        Integer int1 = (Integer)a;
        Integer int2 = (Integer)b;
        return int1 < int2;
    }
}
