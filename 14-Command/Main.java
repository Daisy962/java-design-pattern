public class Main {
    public static void main(String[] args){
        new Main().run();
    }

    private ThreadInvoker invoker;

    public void run(){
        // 事件发生器
        invoker = new ThreadInvoker();
        invoker.start();

        // 请求命令
        invoker.putCommand(new PrintCommand(this, "hello world"));
        invoker.putCommand(new PrintCommand(this, "second message"));
        invoker.putCommand(new ExitCommand(this));
    }

    public void onPrint(String message){
        System.out.println(message);
    }

    public void onExit() {
        System.out.println("Exiting");
        invoker.interrupt();
    }
}

