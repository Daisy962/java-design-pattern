public class DivideExpression implements Expression{
    private Expression a;
    private Expression b;

    public DivideExpression(Expression a, Expression b){
        this.a=a;
        this.b=b;
    }

    @Override
    public double eval(Context context){
        double a = this.a.eval(context);
        double b = this.b.eval(context);
        return a / b;
    }
}
