public class iPhoneBuilderImpl implements iPhoneBuilder{
    private iPhoneImpl result = new iPhoneImpl();

    @Override
    public void addCpu(String cpu){
        this.result.addConfig("CPU:"+cpu);
    }

    @Override
    public void addRam(String ram){
        this.result.addConfig("RAM:"+ram);
    }

    @Override
    public void addScreen(String screen){
        this.result.addConfig("Screen:"+screen);
    }

    @Override
    public iPhone build(){
        return this.result;
    }
}
