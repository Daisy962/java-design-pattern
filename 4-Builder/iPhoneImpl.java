import java.util.List;
import java.util.LinkedList;

public class iPhoneImpl implements iPhone {
    private List<String> config = new LinkedList<String>();

    public void addConfig(String config){
        this.config.add(config);
    }

    @Override
    public void show(){
        System.out.println(this.config);
    }
}
