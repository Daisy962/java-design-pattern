public class Main{
    public static void main(String[] argv){
        //生成责任链
        Handler handler = null;
        handler = new ClickHandler(handler);
        handler = new MouseDownHandler(handler);
        handler = new MouseUpHandler(handler);
        
        //设置事件发生器
        MainWindow window = new MainWindow();
        window.setEventHandler(handler);

        //产生点击事件
        window.click();
    }
}
