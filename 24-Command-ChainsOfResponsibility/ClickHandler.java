public class ClickHandler implements Handler {
    private Handler next;

    public ClickHandler(Handler next){
        this.next = next;
    }

    public void handle(Event event){
        String type = event.getType();
        if(type.equals("click")){
            System.out.println("Handle click event");
        } else {
            if(this.next!=null){
                this.next.handle(event);
            }
        }
    }
}
