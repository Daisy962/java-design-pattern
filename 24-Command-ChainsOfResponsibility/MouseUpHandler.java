public class MouseUpHandler implements Handler{
    private Handler next;

    public MouseUpHandler(Handler handler){
        this.next = handler;
    }

    @Override
    public void handle(Event event){
        String type = event.getType();
        if(type.equals("mouse-up")){
            System.out.println("mouse up handled");
        } else{
            if(this.next!=null){
                this.next.handle(event);
            }
        }
    }
}
