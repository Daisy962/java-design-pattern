public class LinuxWindow implements Window{
    @Override
    public void addButton(String message){
        System.out.println("Linux window add button: " + message);
    }

    @Override
    public void addInput(String message){
        System.out.println("Linux window add input: " + message);
    }

    @Override
    public void show(){
        System.out.println("Linux window show");
    }
}
