public class File implements Node {
    private String name;
    private String content;

    public File(String name){
        this.name = name;
    }

    @Override
    public String getFileName(){
        return name;
    }

    public String read(){
        return content;
    }

    public void write(String content){
        this.content = content;
    }
}
