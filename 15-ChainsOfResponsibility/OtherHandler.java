public class OtherHandler implements Handler{
    private Handler next;

    public OtherHandler(Handler handler){
        this.next=  handler;
    }

    @Override
    public void handle(String string){
        System.out.println("OtherHandler: " + string);
    }
}
