public class ButtonImpl implements Button{
    private Window window;

    public ButtonImpl(Window window){
        this.window = window;
    }

    public void disable(){
        System.out.println("Button is disabled");
    }

    public void enable(){
        System.out.println("Button is enabled");
    }
}
