public class LabelImpl implements Label{
    private Window window;

    public LabelImpl(Window window){
        this.window = window;
    }

    @Override
    public void setText(String text){
        System.out.println(String.format("Label text is set to %s", text));
    }
}
