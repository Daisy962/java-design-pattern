public class PercentageDiscountStrategy implements DiscountStrategy{
    private float discount;

    public PercentageDiscountStrategy(float discount){
        this.discount = discount;
    }

    @Override
    public float getPrice(float price){
        return this.discount * price;
    }
}
