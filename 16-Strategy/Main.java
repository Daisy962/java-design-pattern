public class Main{
    public static void main(String[] argv){
        //没有折扣
        Context context1 = new Context(new NoDiscountStrategy());
        float price1 = context1.getPrice(100);
        System.out.println(String.format("NoDiscount: %f", price1));

        //百分比折扣
        Context context2 = new Context(new PercentageDiscountStrategy(0.3f));
        float price2 = context2.getPrice(100);
        System.out.println(String.format("Percentage: %f", price2));
    }
}
