public class Context{
    private DiscountStrategy discount;

    public Context(DiscountStrategy discount){
        this.discount=discount;
    }

    public float getPrice(float origin){
        return this.discount.getPrice(origin);
    }
}
