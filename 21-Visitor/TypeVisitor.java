public class TypeVisitor implements ComputerVisitor{
    @Override
    public void visit(CPU cpu, Harddisk harddisk){
        System.out.println("CPU type is " + cpu.getType());
        System.out.println("Harddisk type is " + harddisk.getType());
    }
}
