public class Harddisk {
    private String type;

    public Harddisk(String type){
        this.type = type;
    }

    public String getType(){
        return type;
    }

    //CPU中运行的接口是run，但是硬盘的接口为spin
    //虽然两者接口不一致，但是Visitor模式可以解决这种问题
    public void spin(){
        System.out.println(String.format("Harddisk %s is spin", this.type));
    }
}
