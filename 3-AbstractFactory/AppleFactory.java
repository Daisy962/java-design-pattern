public interface AppleFactory {
    public static final int IPHONE_4 = 1;
    public static final int IPHONE_5 = 2;

    public static final int IPAD_4 = 1;
    public static final int IPAD_AIR = 2;

    public iPhone createiPhone(int iPhoneVersion);
    public iPad createiPad(int iPadVersion);
}
