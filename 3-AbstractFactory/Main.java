public class Main{
    public static void main(String[] argv){
        // 建立苹果公司
        AppleFactory factory = new AppleFactoryImpl();

        // 生产iPhone
        iPhone ip4 = factory.createiPhone(AppleFactory.IPHONE_4);
        iPhone ip5 = factory.createiPhone(AppleFactory.IPHONE_5);
        ip4.turnOn();
        ip5.turnOn();

        // 生产iPad
        iPad ipad4 = factory.createiPad(AppleFactory.IPAD_4);
        iPad ipadair = factory.createiPad(AppleFactory.IPAD_AIR);
        ipad4.charge();
        ipadair.charge();
    }
}
