import java.util.List;

public class DataState{
    private List<String> list;

    public DataState(List<String> list){
        this.list = list;
    }

    public List<String> getList(){
        return list;
    }
}
