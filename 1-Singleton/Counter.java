public class Counter {
    private static Counter instance = new Counter();

    private int count = 0;

    public static Counter getInstance(){
        return instance;
    }

    public synchronized int tick(){
        this.count++;
        return this.count;
    }

    private Counter(){}
}
