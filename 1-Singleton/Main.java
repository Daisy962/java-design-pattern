public class Main {

    public static void main(String[] argv) {
        // 获取计数器
        final Counter counter = Counter.getInstance();
        
        // 创建5条线程同时操作计数器
        for( int i = 0; i < 5; i++) {
            new Thread( new Runnable() {
                @Override
                public void run(){
                    for( int j = 0; j < 100; j++ ) {
                        System.out.println( counter.tick() );
                    }
                }
            }).start();
        }
    }
}
