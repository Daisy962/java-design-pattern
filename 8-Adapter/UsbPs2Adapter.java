public class UsbPs2Adapter implements PS2Interface{
    private USBInterface usb;

    public UsbPs2Adapter(USBInterface usb){
        this.usb=usb;
    }

    @Override
    public void connectPS2(){
        this.usb.connectUSB();
    }
}
