public class Main{
    public static void main(String[] args){
        Secretary sec = new Secretary();
        Boss boss = new Boss(sec);
        boss.run();
    }
}
